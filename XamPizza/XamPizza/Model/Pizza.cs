﻿using System;
using System.Collections.Generic;
using System.Text;
using XamPizza.Extension;

namespace XamPizza.Model
{


    public class Pizza
    {

        public string nom { get; set; }
        public int prix { get; set; }
        public string[] ingredients { get; set; }
        public string imageUrl { get; set; }


        public string PrixEuros { get { return prix + "€"; } }
        public string IngredientsStr { get { return String.Join(", ", ingredients); } }

        public string Titre { get { return nom.PremiereLettreEnMajuscule(); } }

    }
}
