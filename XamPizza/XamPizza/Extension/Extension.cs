﻿using System;
using System.Collections.Generic;
using System.Text;

namespace XamPizza.Extension
{
    public static class Extension
    {


        public static string PremiereLettreEnMajuscule(this string str)
        {
            if (String.IsNullOrEmpty(str))
            {


                return str;
            }

            string ret = str.ToLower();


            ret = ret.Substring(0, 1).ToUpper() + ret.Substring(1, ret.Length - 1);

            return ret;



        }




    }
}
